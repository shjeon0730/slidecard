
import React, {Component} from 'react';
import SlideCard from './slide-card/SlideCard';
import "./App.css";

class App extends Component {
  constructor(props){
    super(props);
    this.number = 1;
    this.state = {
      newCard: null
    }
  }
  

  getRandomCard=()=>{
    const color = (this.number%2 === 0)? "#ff3333" : "#33ff33";
    return (
      <div className="card" style={{backgroundColor: color}}>
        <h3>{this.number++}</h3>
      </div>
    );
  }

  genNewCard = ()=>{
    this.setState({newCard: this.getRandomCard()});
  }
  render(){
    return (
      <div className="container" onClick={()=>{this.genNewCard()}}>
        <SlideCard>
          {this.state.newCard || this.getRandomCard()}
        </SlideCard>
      </div>
    );
  }
  
}

export default App;
