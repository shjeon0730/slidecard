import React, {Component} from 'react';
import './slide-card.css';
import PropTypes from 'prop-types';

class SlideCardChild extends Component {
    componentDidUpdate() {
        if (this.props.slide === true) {
            setTimeout(
                obj => {
                    if (obj.slideDiv && obj.leaveDiv) {
                        let newClass = 'slide-card';

                        if (obj.props.direction) {
                            newClass += ' active';
                            if (
                                typeof 'str' !== typeof obj.props.direction &&
                                obj.props.direction.length &&
                                obj.props.direction.length > 0
                            ) {
                                obj.props.direction.forEach((dir, i, arr) => {
                                    newClass += ' ' + dir;
                                });
                            } else {
                                newClass += ' ' + obj.props.direction;
                            }
                        } else {
                            newClass += ' active left';
                        }

                        obj.slideDiv.className = newClass;
                        obj.leaveDiv.className = 'leave-card-from';
                    }
                },
                1,
                this
            );
            setTimeout(
                obj => {
                    if (obj.leaveDiv) {
                        obj.leaveDiv.className = 'leave-card-to';
                    }
                },
                100,
                this
            );
            setTimeout(
                obj => {
                    if (obj.leaveDiv) {
                        obj.leaveDiv.className="leave-card";
                    }
                    obj.callSlideFinished();
                },
                500,
                this
            );
        }
    }

    callSlideFinished = () => {
        if (this.props.onSlideFinished) this.props.onSlideFinished();
    };

    getSlideCards = () => {
        console.log('getSlideCards');
        return (
            <React.Fragment>
                <div 
                    ref={ref => {
                        this.leaveDiv = ref;
                    }}
                    className="leave-card"
                >
                    {this.props.card}
                </div>
                <div
                    ref={ref => {
                        this.slideDiv = ref;
                    }}
                    className="slide-card"
                >
                    {this.props.oldCard}
                </div>
            </React.Fragment>
        );
    };

    getNoSlideCard = () => {
        console.log('getNoSlideCard');
        return <div className="leave-card">{this.props.card}</div>;
    };

    render() {
        console.log('child is slide?' + this.props.slide);
        return (
            <div className="slide-card-container">
                {this.props.slide === true ? this.getSlideCards() : this.getNoSlideCard()}
            </div>
        );
    }
}
SlideCardChild.propTypes = {
    card: PropTypes.element,
    direction: PropTypes.string,
    oldCard: PropTypes.element,
    onSlideFinished: PropTypes.func,
    slide: PropTypes.bool
};

export default SlideCardChild;
