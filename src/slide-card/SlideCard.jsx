import React, {Component} from 'react';
import Child from './SlideCardChild';

class SlideCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            slide: false,
            card: props.children,
            oldCard: null,
            slideFinished: false
        };
    }

    static getDerivedStateFromProps(props, state) {
        if (props.children === state.card) {
            return {
                ...state,
                oldCard: null,
                slide: false,
                slideFinished: false
            };
        } else {
            return {
                ...state,
                oldCard: state.card,
                card: props.children,
                slide: true,
                slideFinished: false
            };
        }
    }

    onSlideFinished = () => {
        this.setState({
            slideFinished: true,
            slide: false
        });
    };

    render() {
        const {children: _, ...newProps} = this.props;
        const childProps = {
            ...newProps,
            slide: this.state.slide,
            card: this.state.card,
            oldCard: this.state.oldCard,
            onSlideFinished: this.onSlideFinished
        };
        return <Child {...childProps} />;
    }
}

export default SlideCard;
